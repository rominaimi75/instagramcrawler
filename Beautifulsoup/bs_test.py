from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
import time
from time import sleep
from bs4 import BeautifulSoup
import json
from soup2dict import convert
from tqdm import tqdm


# Instagram Login Function
def login():
    PATH = r"C:\Users\asus\Downloads\chromedriver.exe"
    driver = webdriver.Chrome(PATH)

    driver.get("https://www.instagram.com/")
    time.sleep(5)

    username = driver.find_element_by_css_selector("input[name='username']")
    password = driver.find_element_by_css_selector("input[name='password']")
    username.clear()
    password.clear()
    # username.send_keys("great.year1375")
    # password.send_keys("ro444575")
    username.send_keys("doesnotrelatedtoyou")
    password.send_keys("1234567890test")

    login = driver.find_element_by_css_selector("button[type='submit']").click()
    sleep(10)

    return driver


# Function for UserInfo
def user_info(target_page):
    page_source = target_page.prettify()
    # print(page_source)
    page_name = target_page.find_all('h1', {'class': "rhpdm"})[0].text
    # print(page_name)
    follower_following_post = target_page.find_all('li', attrs={'class': {"Y8-fY"}})
    # follower_following_link = target_page.find_all('li', attrs={'class': {"Y8-fY"}}, href=True)
    # print(follower_following_link)
    count_list = []
    for item in follower_following_post:
        count_list.append(item.text)
        # print(item.text)
    post_count = count_list[0]
    follower_count = count_list[1]
    following_count = count_list[2]

    user = {
        "bio": page_name,
        "post": post_count,
        "follower": follower_count,
        "following": following_count
    }
    # print(user)
    # print('done')
    return user


# Function for PostsInfo
def post_info(target_page):
    posts = target_page.find_all("div", {"class": "KL4Bh"})
    post_list = []
    for post in posts:
        post_dictionary = convert(post)
        image_info = post_dictionary['img']
        print(image_info)
        if '@alt' in image_info[0]:
            post_description = image_info[0]['@alt']
        post_media_link = image_info[0]['@src']
        post_information = {'description': post_description,
                            'media_link': post_media_link}
        # print(post_information)
        post_list.append(post_information)
    return post_list
    # for posts in User_posts:
    #     post_description=
    #     post_url=
    #     image_url=
    #
    #     # add to the end of PostInfo list with append
    #     PostInfo.append([{
    #                       "PostDescription": post_description,
    #                       "ImageUrl": image_url,
    #                       "PostUrl": post_url
    #                       }])


def write_result_to_file(data):
    with open('../kafka/instagram_data.json', 'w') as file:
        json.dump(data, file, indent=3)
# get target page function
def get_target_page(driver, target_list):
    try:
        bs_data = {'bs_data': []}
        for user in target_list:
            driver.get("https://www.instagram.com/" + user)

            # wait for page to load
            WebDriverWait(driver, 30).until(lambda x: x.find_element_by_css_selector("div[class='_9AhH0']"))

            # scrolling down the page
            last_height = driver.execute_script("return document.body.scrollHeight")
            SCROLL_PAUSE_TIME = 10
            with tqdm() as pbar:
                while True:
                    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
                    time.sleep(SCROLL_PAUSE_TIME)
                    new_height = driver.execute_script("return document.body.scrollHeight")
                    if new_height == last_height:
                        break
                    last_height = new_height
                    pbar.update()
            # scroll done
            html_source = driver.page_source
            sourcedata = html_source.encode('utf-8')
            soup = BeautifulSoup(sourcedata, 'html.parser')

            info = user_info(target_page=soup)
            info['username'] = user
            posts = post_info(target_page=soup)
            info['posts'] = posts
            # print(info)
            print('got user data')
            bs_data['bs_data'].append(info)
        # save results
        write_result_to_file(bs_data)
    except Exception as err:
        print(err)


if __name__ == "__main__":
    target_list = ["fast.coffee59","sabawv_art"]
    connected_driver = login()
    get_target_page(connected_driver, target_list)
    connected_driver.quit()
