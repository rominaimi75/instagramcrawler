from time import sleep
import threading
from json import loads
from kafka import KafkaConsumer


class Consumer(threading.Thread):
    # Initialize Consumer
    def __init__(self):
        super(Consumer, self).__init__()
        self.consumer = KafkaConsumer('rawdata',
                                      bootstrap_servers=['localhost:9092'],
                                      auto_offset_reset='earliest',
                                      enable_auto_commit=True,
                                      # take a raw message value and returns a deserialized value
                                      value_deserializer=lambda c:
                                      loads(c.decode('utf-8'))
                                      )

    # message receiver function
    def run(self):
        for data in self.consumer:
            message = data.value
            print(message)
            sleep(5)
