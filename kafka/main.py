from kafka.consumer import Consumer
from kafka.producer import Producer

if __name__ == '__main__':
    producer = Producer()
    consumer = Consumer()
    threads = [producer, consumer]
    for thread in threads:
        thread.start()

