from time import sleep
import json
from json import dumps
import threading
from kafka import KafkaProducer


class Producer(threading.Thread):
    # Initialize Producer
    def __init__(self):
        super(Producer, self).__init__()
        self.producer = KafkaProducer(bootstrap_servers=['localhost:9092'],
                                      # convert user-supplied message values to bytes
                                      value_serializer=lambda x:
                                      dumps(x).encode('utf-8'))
        self.file = open('instagram_data.json')

    # message publisher function
    def run(self):
        data = json.load(self.file)
        self.producer.send('rawdata', value=data)
        sleep(3)
